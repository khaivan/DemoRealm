package com.example.khaicoder.demorealm;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.khaicoder.demorealm.model.Demo;

import io.realm.Realm;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Realm realm;
    private TextView tv;
    private EditText editText, edtId;
    private String str = "";
    private Demo demo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tv = findViewById(R.id.tv);
        editText = findViewById(R.id.edt);

        realm = Realm.getDefaultInstance();
        findViewById(R.id.btn).setOnClickListener(this);

        findViewById(R.id.btn_update).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str = editText.getText().toString();
                realm.beginTransaction();
                demo = new Demo();
                demo.setText(str);
                update(demo);
            }
        });
        findViewById(R.id.btn_fre).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fre();

            }
        });

        findViewById(R.id.btn_del).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str = editText.getText().toString();
                delete(str);
            }
        });

    }

    private void update(Demo str) {

        RealmResults<Demo> demos = realm.where(Demo.class).equalTo("text", str.getText()).findAll();
        realm.insertOrUpdate(demos);
        realm.commitTransaction();
    }

    private void delete(String name) {
        realm.beginTransaction();
        RealmResults<Demo> demos = realm.where(Demo.class).equalTo("text", name).findAll();
        demos.deleteAllFromRealm();

        realm.commitTransaction();
        show();
    }

    private void show() {
        realm.beginTransaction();
        RealmResults<Demo>demos = realm.where(Demo.class).findAll();
        String str ="";
        for (Demo de:demos
             ) {
            str+=de.getText();
        }
        tv.setText(str);
        realm.commitTransaction();
    }

    private void fre() {
        realm.beginTransaction();
        realm.deleteAll();
        realm.commitTransaction();
        show();
    }


//    private void getRe() {
//        realm.beginTransaction();
//        RealmResults<Demo> demos = realm.where(Demo.class).findAll();
//        String str = "";
//        for (Demo de : demos
//                ) {
//            str += de.getText();
//
//        }
//        tv.setText(str);
//        realm.commitTransaction();
//    }

    private void inser() {
        realm.beginTransaction();
        demo = realm.createObject(Demo.class);
        demo.setText(editText.getText().toString());
        realm.commitTransaction();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    @Override
    public void onClick(View v) {
        inser();
        show();
    }
}
