package com.example.khaicoder.demorealm.model;

import io.realm.RealmObject;

public class Demo extends RealmObject {
    private String text;

    public Demo() {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
